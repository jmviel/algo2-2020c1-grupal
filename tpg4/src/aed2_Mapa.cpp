#include "aed2_Mapa.h"

// Completar
aed2_Mapa::aed2_Mapa() : horizontales_() , verticales_() {}

void aed2_Mapa::agregarRio(Direccion d, int p) {
    if(d == Horizontal){
        horizontales_.push_back(p);
    }
    if(d == Vertical){
        verticales_.push_back(p);
    }
}


bool aed2_Mapa::hayRio(Casilla c) const {
    for(int n: horizontales_){
        if(n == c.second){
            return true;
        }
    }
    for(int n: verticales_){
        if(n == c.first){
            return true;
        }
    }
    return false;
}

void aed2_Mapa::unirMapa(aed2_Mapa m2) {
    this->horizontales_.splice(this->horizontales_.end(), m2.horizontales());
    this->verticales_.splice(this->verticales_.end(), m2.verticales());
}

list<int> aed2_Mapa::horizontales() {
    return horizontales_;
}

list<int> aed2_Mapa::verticales() {
    return verticales_;
}

Mapa aed2_Mapa::mapa(){
    Mapa m;
    m.first=this->verticales_;
    m.second=this->horizontales_;
    return m;
}

aed2_Mapa::aed2_Mapa(Mapa m){
    this->horizontales_=m.second;
    this->verticales_=m.first;
}
