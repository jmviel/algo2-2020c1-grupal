#include "aed2_SimCity.h"

// Completar

aed2_SimCity::aed2_SimCity(aed2_Mapa m) : comercios_(),
                                          casas_(),
                                          huboConstruccion_(false),
                                          popularidad_(0),
                                          turnoActual_(0),
                                          mapa_(m.mapa()) {};

void aed2_SimCity::agregarCasa(Casilla c) {
    casas_.push_back(make_pair(c,0));
    huboConstruccion_ = true;
}

void aed2_SimCity::agregarComercio(Casilla c) {
    comercios_.push_back(make_pair(c,0));
    huboConstruccion_ = true;
}

void aed2_SimCity::avanzarTurno(){
    asignarNivelComercios();
    limpiarCasas();
    limpiarComercios();
    casas_.unique();
    comercios_.unique();
    subirNiveles();
    turnoActual_++;
    huboConstruccion_ = false;
}

void aed2_SimCity::unir(aed2_SimCity sc) {
    this->mapa_.unirMapa(sc.mapa_);
    this->casas_.splice(this->casas_.end(), sc.casas_);
    this->comercios_.splice(this->comercios_.end(), sc.comercios_);
    this->popularidad_ = this->popularidad_ + sc.popularidad() + 1;
    this->huboConstruccion_ = this->huboConstruccion_ || sc.huboConstruccion_;
    if(sc.antiguedad() > this->turnoActual_){
        turnoActual_ = sc.antiguedad();
    }
}

aed2_Mapa aed2_SimCity::mapa() const {
    return mapa_;
}

set<Casilla> aed2_SimCity::casas() {
    asignarNivelComercios();
    limpiarCasas();
    limpiarComercios();
    casas_.unique();
    comercios_.unique();
    set<Casilla> casas;
    for(pair<Casilla, Nat> p : casas_){
        casas.insert(p.first);
    }
    return casas;
}

set<Casilla> aed2_SimCity::comercios() {
    asignarNivelComercios();
    limpiarCasas();
    limpiarComercios();
    casas_.unique();
    comercios_.unique();
    set<Casilla> comercios;
    for(pair<Casilla, Nat> p : comercios_){
        comercios.insert(p.first);
    }
    return comercios;
}

Nat aed2_SimCity::nivel(Casilla c) {
    asignarNivelComercios();
    limpiarCasas();
    limpiarComercios();
    casas_.unique();
    comercios_.unique();
    for(pair<Casilla, Nat> p : casas_){
        if(p.first == c){
            return p.second;
        }
    }
    for(pair<Casilla, Nat> p : comercios_){
        if(p.first == c){
            return p.second;
        }
    }
}

bool aed2_SimCity::huboConstruccion() const {
    return huboConstruccion_;
}

Nat aed2_SimCity::popularidad() const {
    return popularidad_;
}

Nat aed2_SimCity::antiguedad() const {
    return turnoActual_;
}

void aed2_SimCity::limpiarCasas() {
    list<pair<Casilla,Nat>> l;
    for(pair<Casilla,Nat> c : casas_){
        Nat max = c.second;
        for(pair<Casilla,Nat> p : casas_){
            if(c.first == p.first && p.second > c.second){
                max = p.second;
            }
        }
        l.push_back(make_pair(c.first,max));
    }
    casas_ = l;
}

void aed2_SimCity::limpiarComercios() {
    list<pair<Casilla, Nat>> l;
    for (pair<Casilla, Nat> c : comercios_) {
        Nat max = c.second;
        for (pair<Casilla, Nat> p : comercios_) {
            if (c.first == p.first && p.second > c.second) {
                max = p.second;
            }
        }
        bool estaEnCasas = false;
        for (pair<Casilla, Nat> q : casas_) {
            if(c.first == q.first){
                estaEnCasas = true;
            }
        }
        if (!estaEnCasas) {
            l.push_back(make_pair(c.first, max));
        }
    }
    comercios_ = l;
}

void aed2_SimCity::subirNiveles() {
    list<pair<Casilla,Nat>> l1;
    for(pair<Casilla,Nat> ca : casas_){
        Casilla aux = ca.first;
        Nat naux = ca.second + 1;
        l1.push_back(make_pair(aux,naux));
    }
    casas_ = l1;
    list<pair<Casilla,Nat>> l2;
    for(pair<Casilla,Nat> co : comercios_){
        Casilla aux = co.first;
        Nat naux = co.second + 1;
        l2.push_back(make_pair(aux,naux));
    }
    comercios_ = l2;
}

Nat aed2_SimCity::distManh(Casilla c1, Casilla c2) const {
    return abs(c1.first - c2.first) + abs(c1.second - c2.second);
}

Nat aed2_SimCity::nivelMax(Casilla c) const {
    Nat max = 0;
    for(pair<Casilla, Nat> p : casas_){
        if(distManh(c, p.first) <= 3 && p.second > max){
            max = p.second;
        }
    }
    return max;
}

void aed2_SimCity::asignarNivelComercios() {
    list<pair<Casilla,Nat>> l;
    for(pair<Casilla,Nat> p: comercios_){
        if(p.second < nivelMax(p.first)){
            l.push_back(make_pair(p.first,nivelMax(p.first)));
        }else{
            l.push_back(p);
        }
    }
    comercios_ = l;
}

