#ifndef AED2_SIMCITY_H
#define AED2_SIMCITY_H

#include <iostream>
#include <set>
#include "aed2_Mapa.h"
#include <list>

using namespace std;

using Nat = unsigned int;

//using SimCity = pair<list<int>, list<int>>; /* Completar con su clase implementada de SimCity */

class aed2_SimCity {
public:

    // Generadores:

    aed2_SimCity(aed2_Mapa m);

    // Precondicion: Se puede construir en la Casilla c
    void agregarCasa(Casilla c);

    // Precondicion: Se puede construir en la Casilla c
    void agregarComercio(Casilla c);

    // Precondicion: Hubo construccion en el turno actual
    void avanzarTurno();

    // Precondicion: No se solapan las construcciones con los rios
    //  ni las construcciones de nivel maximo de ambas partidas
    void unir(aed2_SimCity sc);

    // Observadores:

    aed2_Mapa mapa() const;

    set<Casilla> casas();

    set<Casilla> comercios();

    // Precondicion: Hay construccion en la Casilla p
    Nat nivel(Casilla c);

    bool huboConstruccion() const;

    Nat popularidad() const;

    Nat antiguedad() const;

    Nat distManh(Casilla c1, Casilla c2) const;

    Nat nivelMax(Casilla c) const;

    void limpiarCasas();

    void limpiarComercios();

    void asignarNivelComercios();

    void subirNiveles();

    // Conversiones
    
    // Esta función sirve para convertir del SimCity de la cátedra al SimCity
    // implementado por ustedes
    //SimCity simCity();

private:
    // Completar
    list<pair<Casilla,Nat>> comercios_;

    list<pair<Casilla,Nat>> casas_;

    bool huboConstruccion_;

    Nat popularidad_;

    Nat turnoActual_;

 //   Mapa mapa_;
    aed2_Mapa mapa_;
    
};

#endif // AED2_SIMCITY_H
